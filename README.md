# Herding your containers and services

This is a companion repository for the talk "Herding your containers and 
services" by Mike van Riel.

This repository contains the source code for an example project that utilizes
docker to set up a simple PHP application.
